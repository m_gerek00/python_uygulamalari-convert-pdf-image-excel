import os
from typing import List
import openpyxl
from openpyxl.utils import get_column_letter

def text_into_spreadsheet():
    workbook = openpyxl.Workbook()
    sheet = workbook.active
    column: int = 1
    filenames: List[str] = os.listdir() 
    for filename in filenames:
        if filename.endswith(".txt"):
            with open(filename) as textfile:
                lines: List[int] = textfile.readlines()
                sheet[get_column_letter(column) + '1'] = filename
                row: int = 2
                for line in lines:
                    sheet[get_column_letter(column) + str(row)] = line
                    row += 1
            column += 1
    workbook.save('Sonuc.xlsx')

if __name__ == "__main__":
    text_into_spreadsheet()
