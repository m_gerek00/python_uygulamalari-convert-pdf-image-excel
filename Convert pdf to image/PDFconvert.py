from PIL import Image 
import pytesseract 
import sys 
from pdf2image import convert_from_path 
import os 
  
PDF_file = "Dpu_Makale.pdf"
  
''' 
PDF'yi görüntülere dönüştürme

''' 
pages = convert_from_path(PDF_file, 500) 
  
counter = 1
  
for page in pages: 
  
    filename = "page_"+str(counter)+".jpg"
      
    page.save(filename, 'JPEG') 
  
    counter = counter + 1

  
''' 
OCR kullanarak görüntülerden gelen metni tanıma

'''
 
filelimit = counter-1
  
outfile = "output.txt"

f = open(outfile, "a")  #Dosyada degisiklik yapmak!

for i in range(1, filelimit + 1): 
  
    filename = "page_"+str(i)+".jpg" 
           
    text = str(((pytesseract.image_to_string(Image.open(filename))))) #Resimden text haline donus

    text = text.replace('-\n', '')     
  
    f.write(text) 
  
f.close() 
