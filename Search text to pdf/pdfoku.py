import PyPDF2
from PyPDF2 import PdfFileReader

pdfFileObj = open('Dpu_Makale.pdf', 'rb')

pdfReader = PyPDF2.PdfFileReader(pdfFileObj)

search_word = "ret"
count = 0

for PageNum in range(0,pdfReader.numPages):
    page = pdfReader.getPage(PageNum)
    text = page.extractText().encode('utf-8')
    search_txt = text.lower().split()
    for word in search_txt:
        if search_word in word.decode("utf-8"):
            count += 1


print("Aranan Kelime",count,"kez bulundu".format(search_word,count))     
